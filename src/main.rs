#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate toml;
#[macro_use]
extern crate rouille;
#[macro_use]
extern crate failure;
extern crate rusqlite;
extern crate time;
extern crate xml;

use std::io;

use rouille::{Request, Response};

use failure::Error;

mod vocabdb;
mod xmlify;
use vocabdb::VocabDB;

type ServerResult<T> = Result<T, Error>;

#[derive(Deserialize, Clone)]
struct ServerConfig {
    address: String,
    username: String,
    password: String,
}

fn cleanup_input(input: &str) -> String {
    input.trim().to_string()
}

fn api_get(request: &Request) -> ServerResult<Response> {
    if let Some(nstr) = request.get_param("list") {
        let count = nstr.parse::<i32>()?;
        let db = VocabDB::new_connection()?;
        let entries = db.get_entries_by_value(count)?;
        let entrycount = db.get_entrycount()?;
        return Ok(Response::from_data(
            "text/xml",
            xmlify::entries(&entries, entrycount)?,
        ));
    } else if let Some(nstr) = request.get_param("history") {
        let count = nstr.parse::<i32>()?;
        let db = VocabDB::new_connection()?;
        let entries = db.get_newest_transactions(count)?;
        let entrycount = db.get_entrycount()?;
        return Ok(Response::from_data(
            "text/xml",
            xmlify::entries(&entries, entrycount)?,
        ));
    }

    Err(format_err!("wrong get-api usage"))
}

fn api_post(request: &Request) -> ServerResult<Response> {
    let input = post_input!(request, {
                        txt: String,
						value: i32,
                        comment: Option<String>
                    })?;
    let txt = cleanup_input(&input.txt);
    if txt.is_empty() {
        return Ok(Response::empty_204());
    }

    let mut comment = None;
    if let Some(c) = input.comment.map(|s| cleanup_input(&s)) {
        if !c.is_empty() {
            comment = Some(c);
        }
    }

    let db = VocabDB::new_connection()?;
    db.add_transaction(time::get_time(), &txt, input.value, comment)?;
    Ok(Response::empty_204())
}

fn api(request: &Request) -> ServerResult<Response> {
    match request.method() {
        "GET" => api_get(request),
        "POST" => api_post(request),
        _ => Err(format_err!("wrong api usage")),
    }
}

fn read_server_config() -> ServerResult<ServerConfig> {
    let cfg_file = std::fs::read_to_string("data/server.toml")?;
    let server_config: ServerConfig = toml::from_str(&cfg_file)?;
    Ok(server_config)
}

fn route_request(request: &Request) -> ServerResult<Response> {
    router!(request,
    (GET) (/) => {
        Ok(Response::from_file("text/html", std::fs::File::open("data/public/index.html")?))
        },
    (GET) (/api) => { api(request) },
    (POST) (/api) => { api(request) },
    _ => Ok(rouille::match_assets(&request, "data/public/"))
)
}

fn request_handler(request: &Request) -> Response {
    rouille::log(request, io::stdout(), || {
        let server_config = try_or_404!(read_server_config());

        let mut permited = server_config.username.is_empty() && server_config.password.is_empty();
        if !permited {
            if let Some(auth) = rouille::input::basic_http_auth(request) {
                permited =
                    auth.login == server_config.username && auth.password == server_config.password;
            };
        }

        if !permited {
            return Response::basic_http_auth_login_required("vocabdump");
        }

        match route_request(request) {
            Ok(response) => response,
            Err(e) => {
                println!("{}", e);
                Response::empty_400()
            }
        }
    })
}

fn run_server() -> ServerResult<()> {
    VocabDB::init_db()?;

    let server_config = read_server_config()?;
    println!("server listening on http://{}", server_config.address);

    rouille::start_server(&server_config.address, request_handler);
}

fn main() {
    run_server().unwrap_or_else(|e| println!("{}", e));
}

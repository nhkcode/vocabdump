use failure::Error;
use rusqlite::Connection;
use std::vec::Vec;
use time::Timespec;

type DBResult<T> = Result<T, Error>;

pub struct VocabDB {
    connection: Connection,
}

#[derive(Debug)]
pub struct Entry {
    pub word: String,
    pub value: i32,
    pub comments: Vec<String>,
}

struct QueueEntry {
    pub word: String,
    pub value: i32,
    pub comment: Option<String>,
}

impl VocabDB {
    pub fn init_db() -> DBResult<()> {
        let db_file_name = "database.sqlite";
        println!("Initializing database: {}", db_file_name);
        let connection = Connection::open(db_file_name)?;
        connection.execute(
            "CREATE TABLE IF NOT EXISTS transactions (
                id              INTEGER PRIMARY KEY,
                time            TEXT NOT NULL,
                word            TEXT NOT NULL,
                value           INTEGER,
                comment         TEXT
                )",
            &[],
        )?;

        connection.execute(
            "CREATE TABLE IF NOT EXISTS entries (
                word            TEXT NOT NULL PRIMARY KEY,
                value           INTEGER
                )",
            &[],
        )?;

        Ok(())
    }

    pub fn new_connection() -> DBResult<VocabDB> {
        let connection = Connection::open("database.sqlite")?;
        Ok(VocabDB { connection })
    }

    fn update_entry(&self, word: &str, value: i32) -> DBResult<()> {
        self.connection.execute(
            "INSERT OR IGNORE INTO entries (word, value) VALUES(?1, 0)",
            &[&word],
        )?;

        if value == 0 {
            self.connection
                .execute("UPDATE entries SET value = 0 WHERE word = ?1", &[&word])?;
        } else {
            self.connection.execute(
                "UPDATE entries SET value = value + ?1 WHERE word = ?2",
                &[&value, &word],
            )?;
        }

        Ok(())
    }

    pub fn add_transaction(
        &self,
        time: Timespec,
        word: &str,
        value: i32,
        comment: Option<String>,
    ) -> Result<(), Error> {
        println!("add_transaction '{}' {} {:?}", word, value, &comment);

        self.connection.execute(
            "INSERT INTO transactions (time, word, value, comment)
                VALUES (?1, ?2, ?3, ?4)",
            &[&time, &word, &value, &comment],
        )?;

        self.update_entry(&word, value)?;
        Ok(())
    }

    pub fn get_entries_by_value(&self, max_count: i32) -> DBResult<Vec<Entry>> {
        let queue = format!(
            "SELECT DISTINCT entries.word, entries.value, transactions.comment
FROM entries
INNER JOIN transactions ON entries.word  = transactions.word
WHERE entries.value != 0
ORDER BY entries.value DESC, entries.value, transactions.time ASC
LIMIT {}",
            max_count
        );

        let mut statement = self.connection.prepare(&queue)?;

        let entries = statement.query_map(&[], |row| QueueEntry {
            word: row.get(0),
            value: row.get(1),
            comment: row.get(2),
        })?;

        let mut ret = Vec::<Entry>::new();
        for entry in entries {
            let entry = entry?;

            if let Some(last) = ret.last_mut() {
                if last.word == entry.word {
                    if let Some(comment) = entry.comment {
                        last.comments.push(comment);
                    }
                    continue;
                }
            }

            let mut e = Entry {
                word: entry.word,
                value: entry.value,
                comments: Vec::new(),
            };
            if let Some(comment) = entry.comment {
                e.comments.push(comment);
            }
            ret.push(e);
        }

        Ok(ret)
    }

    pub fn get_newest_transactions(&self, max_count: i32) -> DBResult<Vec<Entry>> {
        let queue = format!(
            "SELECT word,value, comment FROM transactions ORDER BY time DESC LIMIT {}",
            max_count
        );

        let mut statement = self.connection.prepare(&queue)?;

        let entries = statement.query_map(&[], |row| QueueEntry {
            word: row.get(0),
            value: row.get(1),
            comment: row.get(2),
        })?;

        let mut ret = Vec::<Entry>::new();
        for entry in entries {
            let entry = entry?;
            let mut e = Entry {
                word: entry.word,
                value: entry.value,
                comments: Vec::new(),
            };
            if let Some(comment) = entry.comment {
                e.comments.push(comment);
            }
            ret.push(e);
        }

        Ok(ret)
    }

    pub fn get_entrycount(&self) -> DBResult<i32> {
        Ok(self.connection.query_row(
            "SELECT DISTINCT COUNT(*) FROM entries WHERE value != 0",
            &[],
            |r| r.get(0),
        )?)
    }
}

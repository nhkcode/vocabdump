use failure::Error;
use std::io;
use vocabdb;
use xml::writer::{EmitterConfig, EventWriter, XmlEvent};

type XMLError<T> = Result<T, Error>;

pub fn entries(entries: &[vocabdb::Entry], totalentrycount: i32) -> XMLError<String> {
    let mut result = Vec::new();
    {
        let mut writer = EmitterConfig::new()
            .perform_indent(true)
            .create_writer(&mut result);

        writer.write(XmlEvent::start_element("entries"))?;

        wrtie_element(&mut writer, "entrycount", &totalentrycount.to_string())?;

        for entry in entries {
            writer.write(XmlEvent::start_element("entry"))?;

            wrtie_element(&mut writer, "word", &entry.word)?;
            wrtie_element(&mut writer, "value", &entry.value.to_string())?;

            write_comments(&mut writer, entry)?;

            writer.write(XmlEvent::end_element())?;
        }
        writer.write(XmlEvent::end_element())?;
    }

    Ok(String::from_utf8(result)?)
}

fn wrtie_element<W: io::Write>(writer: &mut EventWriter<W>, name: &str, s: &str) -> XMLError<()> {
    writer.write(XmlEvent::start_element(name))?;
    writer.write(XmlEvent::characters(s))?;
    writer.write(XmlEvent::end_element())?;
    Ok(())
}

fn write_comments<W: io::Write>(
    writer: &mut EventWriter<W>,
    entry: &vocabdb::Entry,
) -> XMLError<()> {
    if entry.comments.is_empty() {
        return Ok(());
    }

    writer.write(XmlEvent::start_element("comments"))?;
    for comment in &entry.comments {
        wrtie_element(writer, "comment", &comment)?;
    }
    writer.write(XmlEvent::end_element())?;

    Ok(())
}

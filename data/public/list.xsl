<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:param name="showDelButtons">true</xsl:param>

    <xsl:template match="/">

    <div> Total entries: <xsl:value-of select="entries/entrycount"/> </div>

    <table class="history " cellpadding="0" cellspacing="0" border="0">
        <tr>
            <xsl:if test="$showDelButtons = 'true'">
                <th></th>
            </xsl:if>
            <th> grade </th>
            <th> dict </th>
            <th> word </th>
        </tr>
        <xsl:for-each select="entries/entry">

            <xsl:variable name="wordentry">
                <xsl:choose>
                    <xsl:when test="comments">
                        <dl >
                            <dt>
                                <xsl:value-of select="word"/>
                            </dt>
                            <xsl:for-each select="comments/comment">
                                <dd>
                                - <xsl:value-of select="."/>
                                </dd>
                            </xsl:for-each>
                        </dl>
                    </xsl:when>
                    <xsl:otherwise>
                            <xsl:value-of select="word"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>

                <tr>
                    <xsl:if test="$showDelButtons = 'true'">
                        <td>
                            <button title="Delete this entry" onclick="{concat('remove(&quot;', word, '&quot;)')}">X</button>
                        </td>
                    </xsl:if>
                    <td>
                        <xsl:value-of select="value"/>
                    </td>
                    <td>
                        <a href="{concat('http://jisho.org/search/', word)}">jisho</a>
                    </td>
                    <td>
                        <xsl:copy-of select="$wordentry"/>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>

</xsl:stylesheet> 
var listStylesheet;
var xsltProcessor = new XSLTProcessor();

var requestStylesheet = new XMLHttpRequest();
requestStylesheet.open("GET", "list.xsl", false);
requestStylesheet.send(null);
listStylesheet = requestStylesheet.responseXML;
xsltProcessor.importStylesheet(listStylesheet);

var currentlistcount = 10;
var currenthistorycount = 5;

function list(target, n) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            xml = this.responseXML;
            xsltProcessor.setParameter(null, "showDelButtons", "true");
            var fragment = xsltProcessor.transformToFragment(xml, document);

            while (target.firstChild) {
                target.removeChild(target.firstChild);
            }
            target.appendChild(fragment);
            currentlistcount = n;
        }
    };
    xhttp.open("GET", "api?list=" + n);
    xhttp.send();
}
function history(target, n) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            xml = this.responseXML;
            xsltProcessor.setParameter(null, "showDelButtons", "false");
            var fragment = xsltProcessor.transformToFragment(xml, document);

            while (target.firstChild) {
                target.removeChild(target.firstChild);
            }
            target.appendChild(fragment);
            currentlistcount = n;
        }
    };
    xhttp.open("GET", "api?history=" + n);
    xhttp.send();
}

function remove(word) {
    post = "txt=" + word + "&value=" + 0;
    post = encodeURI(post);
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 204) {
            list(vocablist, currentlistcount);
        }
    };
    xhttp.open("POST", "api");
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(post);
}

function input(value) {
    post = "txt=" + txtbox.value + "&value=" + value + "&comment=" + commentbox.value;
    post = encodeURI(post);
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 204) {
            txtbox.value = '';
            commentbox.value = '';
            history(vocablist, currenthistorycount);
        }
    };
    xhttp.open("POST", "api");
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(post);
}
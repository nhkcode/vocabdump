# vocabdump

vocabdump is a simple program for language learners that helps collecting new vocabulary and prioritizing which words to add to a spaced repetition software (SRS) like [Anki](https://apps.ankiweb.net/).

## How to use it
### short version
* Enter word in the upper text box
* Maybe add a comment in the lower text box
* Click one of the buttons, higher numbers for more important words
* Repeat above steps for a lot of words
* On the 'list' page remove words from the top and add them to your SRS

### tl;dr

When learning a language and reading any native level material it can easily happen to encounter an overwhelming amount of new words to learn. Many of those new words may be only very rarely used and it might be better to focus on learning more common words if time for learning is limited. If the new words are added indiscriminately to the SRS one would spend valuable review time on those rare words instead of using it for more 'useful' common words. Frequency rankings of words can help to prioritize words, but the rankings depend on the used corpus to compile them and might not at all be applicable to the material one consumes.

vocabdump helps collecting new words and prioritizing them. Each time a new (or already forgotten) word is encountered it is entered into vocabdump with a grade. When a word is entered multiply times it's grade keeps rising. Over time the more frequent words will be entered more often and rise to the top of the list of all the words entered. Once the list has grown enough one can start picking words off from the top of the list and add them to the SRS.

The grading of words helps with words one deems important, for whatever reason one finds useful. Higher grades for more important ones, lower grades for less important ones.

Comments also accumulate.

## Prebuild packages from CI
[windows x64](https://gitlab.com/nhkcode/vocabdump/-/jobs/artifacts/master/download?job=vocabdump-windows)

[linux x64 statically linked](https://gitlab.com/nhkcode/vocabdump/-/jobs/artifacts/master/download?job=vocabdump-static-linux)

## How to build it

[Rust](https://www.rust-lang.org/) is required to build vocabdump. If you don't have it installed yet either download and install it from [their website](https://www.rust-lang.org/en-US/install.html) or use your distribution's package manager to install it.

Download the repository from the website and unpack it somewhere or use ```git clone https://gitlab.com/nhkcode/vocabdump.git``` to close the repository.

To build it open a terminal and type:

```
cd vocabdump
cargo build --release
```

Now either simply use ```cargo run``` to start it or copy the binary ```target/release/vocabdump``` together with the data folder somewhere else and run vocabdump. On the console it'll print out the url (http://0.0.0.0:8000 by default) of the server. Open that in a web browser to start using it.


## How to configure it

In ```data\server.toml``` a username and password can be defined and the address the webserver listens to can be configured. 

The url of the dictionary that is used to linkify the words can be changed in ```data/public/list.xsl``` by replacing ```http://jisho.org/search/``` with something else.

The look of the web pages can be changes by editing the html, css, and xsl files in ```data\public\```.